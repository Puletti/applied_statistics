# camerino - 07/03/2023
rm( list = ls() )

3*6 == 18

6 > 9

sqrt(2)

library(tidyverse)

2 |> sqrt()

# First R objects ----
## vectors ----
v1 <- c(0, 2, 5) # numeric vector

v2 <- c("a", "b", "c"); v2 # character vector

(v3 <- c(T, T, F, T)) # logical vector

length(v2)

1:20

seq(99, 150, 5)

seq(99, 150, length.out = 10)

### exercises ----
# extract the element in position 4
v_1 <- 11:31

v_1[4]


# Now extract the elements at positions 1, 3 and 6
v_1[c(1, 3, 6)]


a <- seq(99, 150, length.out = 10)
b <- seq(501, 623, by = 7)

c <- c(a, b)

sum(c)
mean(c)
range(c)
min(c)
max(c)

## matrix ----

