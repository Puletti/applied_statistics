# Multivariate analysis of Variance
rm(list = ls())

library(tidyverse)

# 0. recup
df <- iris[, 1:4]

# extract Petal Length of versicolor
PL1 <- iris$Petal.Length[iris$Species=='versicolor']
PL1 |> length() # a vector with 50 numbers

# extract Petal length of virginica
PL2 <- iris$Petal.Length[iris$Species=='virginica']
PL2 |> length() # y contain 50 measurements

# The question is:
# difference between 'sample means' is statistically significative?

PL1 |> hist(breaks = 10)
PL2 |> hist(breaks = 10)

shapiro.test(PL1)
shapiro.test(PL2)

boxplot(PL1, PL2) # a boxplot of the two groups of values
mean(PL1); mean(PL2)

# two sample t-test
t.test(PL1, PL2)
# la diff tra le medie dei due gruppi viene stat sign



# Testing the difference among multiple groups (ANOVA)
# some built-in functions to perform ANOVA

# One way analysis of variance (ANOVA)
# function: aov

# aov(y ~ x, data)

# Esempio: la dimensione del sepalo quanto differisce
# tra le diverse specie di fiori?
# potrebbe servire per utilizzare la variabile
# come discriminante ausiliaria

hist(iris$Sepal.Width)
shapiro.test(iris$Sepal.Width)

library(car)
leveneTest(Sepal.Width ~ Species, data = iris)
# From the output above we can see that the p-value 
# is not less than the significance level of 0.05.
# This means that there is no evidence to suggest 
# that the variance across groups is 
# statistically significantly different.

# Therefore, we can assume the homogeneity of variances 
# in the three different Species.


iris
iris |> 
  ggplot(aes(y = Sepal.Width, x = Species)) +
  geom_boxplot()

library(broom)

iris |> 
  group_by(Species) |> 
  nest() |> 
  mutate(Shapiro = map(data, ~ shapiro.test(.x$Sepal.Width))) |> 
  mutate(glance_shapiro = Shapiro |> map(glance)) |>
  unnest(glance_shapiro)

# The question is:
# se la differenza tra le medie 
# dei TRE campioni sia stat significativa
res.aov <- aov(Sepal.Width ~ Species, data = iris)

summary(res.aov)

# non si sa nulla sulla differenza delle medie nei tre campioni
# presi a coppie, ma solo sul complessivo dei tre: post-hoc

pairwise.t.test(iris$Sepal.Width,
                iris$Species,
                p.adj = "none")

TukeyHSD(res.aov)

